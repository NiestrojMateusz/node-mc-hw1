// Dependencies
const http = require('http');
const { URL, URLSearchParams } = require('url');
const { StringDecoder } = require('string_decoder');
// Instantiate the HTTP server
const server = http.createServer((req,res) => {

  //  Get URL and parse it
  const baseURL = `http://${req.headers.host}${req.url}`;
  const parsedURL = new URL(req.url,baseURL);

  //  Get the path
  const pathname = parsedURL.pathname;

  // Strip off preceeding or trailing slashes
  const trimmedPath = pathname.replace(/^\/+|\/+$/g,'');


  // Get the query sting as an object
  const queryStringObject = new URLSearchParams(parsedURL.searchParams);

  // Get HTTP Method
  const method = req.method.toLowerCase();

  // Get the headers as an object
  const headers = req.headers;

  // Get payload if any
  const decoder = new StringDecoder('utf-8');
  let buffer = '';

  req.on('data', (data) => {
    buffer += decoder.write(data);
  });

  req.on('end', ()=> {
    buffer += decoder.end();

    // Choose the handler this request should go to. if one is not found use the noftFound handeler
    const chosenHandler = typeof(router[trimmedPath]) !== 'undefined' ? router[trimmedPath] : handlers.notFound;

    // Construct the data object to send to the handler

    const data = {
      'trimmedPath': trimmedPath,
      'method': method,
      'payload': buffer,
      'queryStringObject' : queryStringObject,
      'headers' : headers,
    }

    //  Route the request to the handler specified in the router
    chosenHandler(data, (statusCode, payload) => {
      //  Use the status code called back by the handler or default to 200
      statusCode = typeof(statusCode) == 'number' ? statusCode : 200;

      // Use th payload called back by the handler or default ot empty object
      payload = typeof(payload) == 'object' ? payload : {};

      // Convert the payload to a string
      const payloadString = JSON.stringify(payload);

      res.setHeader('Content-Type', 'application/json');
      res.writeHead(statusCode);
      res.end(payloadString);

      console.log('Return this response:', statusCode, payloadString);
    });

  });
});


//  Start the HTTP server, and gave it to listen on port defined in config
server.listen(3000, ()=> {
  console.log(`Server running at  port 3000`)
});

// Instantiate the HTTPS server
// Start the HTTPS server

// All the server logic for both the http and https server
// true means to call query string module




// Define the handlers
const handlers = {}

//  Not found handler
handlers.notFound = function(data, callback) {
  callback(404);
}

handlers.hello = function(data, callback) {
  callback(200, {message: 'Hello World!!!'});
}

// Define a request router
const router = {
  'hello': handlers.hello
}